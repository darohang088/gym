# Use a lightweight base image
FROM nginx:alpine

# Set the working directory to the default Nginx public folder
WORKDIR /usr/share/nginx/html

# Copy the HTML files to the container
COPY . .

# Expose port 80 for the web server
EXPOSE 80

# Command to start Nginx when the container runs
CMD ["nginx", "-g", "daemon off;"]
